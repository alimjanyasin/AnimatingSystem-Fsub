Hypergraph based implementation of System F<: which is the system proprosed 
by the The POPLmark Challenge. Here we took the problem 3 which is implmenting 
System F<: and run some benchmarks (that can be found in https://www.seas.upenn.edu/~plclub/poplmark/).

The code itself contains ideas of how terms, rules are encoded.
To understand how name binding is handlded in such technique, 
please read the "hypergraph representation of lambda terms" and  another paper
which is being ready soon!